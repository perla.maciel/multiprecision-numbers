#include <iostream>
#include <inttypes.h>
#include "rdtsc.h"
#include <stdio.h>
#include <openssl/rand.h>
#include <fstream>
 
typedef struct {
	int size_n;
	long long *numb;
} mp;

//Functions
void add_nc(mp, mp, mp);
void sub_nc(mp, mp, mp);
mp init_mp(int);
void zero(mp);
void one(mp);
void rand_(mp);
long long urand_();
long long orand_();
bool equal_(long long, long long);
bool comp_equal_(mp a, mp b);
bool equal_zero(long long);
bool equal_one(long long);
void print_(long long*);

int main() {
	mp a;
	a = init_mp(4);
	mp b;
	b = init_mp(4);

	//Heating the Processor 
	int heating = 0;
	for (int i = 0; i < 1000; i++) {
		heating++;
	}
	//Result
	mp c;
	c = init_mp(4);
	uint64_t tick = rdtsc();  // tick before

	//ADD
	for (int i = 0; i < 10000; i++) {
		rand_(a);
		rand_(b);
		add_nc(a, b, c);
	}
	//rand_(a);
	//rand_(b);
	//add_nc(a, b, c);
	printf("El numero promedio de cyclos es: %ld\n", (rdtsc() - tick)/10000);
	//printf("%lld\n", a.numb[1]);
	return 0;
}

mp init_mp(int size) {
	mp nm;
	nm.size_n = size;
	nm.numb = new long long[size];
	zero(nm);
	return nm;
}

void add_nc(mp a, mp b, mp c) {
	for (int i = 0; i < a.size_n; i++) {
		c.numb[i] = a.numb[i] + b.numb[i];
		//printf(" [%i]",c.numb[i]);
	}
}

void sub_nc(mp a, mp b, long long* resp) {
	for (int i = 0; i < a.size_n; i++) {
		resp[i] = a.numb[i] + b.numb[i];
		//printf(" [%i]",resp[i]);
	}
}

//Initialize
void zero(mp nm) {
	for (int i = 0; i < nm.size_n; i++) {
		nm.numb[i] = 0;
	}
}

void one(mp nm) {
	nm.numb[0] = 1;
	for (int i = 1; i < nm.size_n; i++) {
		nm.numb[i] = 0;
	}
}

//Random
void rand_(mp nm) {
	for (int i = 0; i < nm.size_n; i++) {
		nm.numb[i] = orand_();
	}
}

//Get Random /dev/urandom
long long urand_(){
	using namespace std;
    long long int random_value = 0; //Declare value to store data into
    size_t size = sizeof(random_value); //Declare size of data
    ifstream urandom("/dev/urandom", ios::in|ios::binary); //Open stream
    if(urandom) //Check if stream is open
    {
        urandom.read(reinterpret_cast<char*>(&random_value), size); //Read from urandom
        if(urandom) //Check if stream is ok, read succeeded
        {
            //std::cout << "Read random value: " << random_value << std::endl;
            return random_value;
        }
        else //Read failed
        {
            return -1;
            //std::cerr << "Failed to read from /dev/urandom" << std::endl;
        }
        urandom.close(); //close stream
    }
    else //Open failed
    {
        return 0;
        //std::cerr << "Failed to open /dev/urandom" << std::endl;
    }
}

//Get Random openssl
long long orand_(){
	long long int random_value = 0;
	RAND_bytes(reinterpret_cast<unsigned char*>(&random_value),8);
	return random_value;
}

//Compare
bool comp_equal_(mp a,mp b) {
	for (int i = 0; i < a.size_n; i++) {
		if (!equal_(a.numb[i], b.numb[i])) {
			return true;
			break;
		}
	}
	return true;
}

bool equal_(long long a, long long b) {
	if (a == b){
		return true;
	}
	else {
		return false;
	}
}

bool equal_zero(long long a) {
	if (a == 0) {
		return true;
	}
	else {
		return false;
	}
}

bool equal_one(long long a) {
	if (a == 1) {
		return true;
	}
	else {
		return false;
	}
}

void print_(long long *n) {
	//printf("0x%" PRId64 "\n",n);
	printf("%016llx",reinterpret_cast<unsigned long long>(n));
}