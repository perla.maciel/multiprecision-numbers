//Source: https://helloacm.com/the-rdtsc-performance-timer-written-in-c/
//  Windows
#ifdef _WIN32

#include <x86intrin.h>
uint64_t rdtsc() {
	return __rdtsc();
}

//  Linux/GCC
#else

uint64_t rdtsc() {
	unsigned int lo, hi;
	__asm__ __volatile__("rdtsc" : "=a" (lo), "=d" (hi));
	return ((uint64_t)hi << 32) | lo;
}

#endif

/*int main(int argc, char* argv[]) {
  uint64_t tick = rdtsc();  // tick before
  for (int i = 1; i < argc; ++ i) {
	system(argv[i]); // start the command
  }
  cout << rdtsc() - tick << endl; // difference
  return 0;
}*/
/*inline unsigned long rdtsc()
{
#if defined(__ia64)

    unsigned long x;
    __asm__ __volatile__("mov &#37;0=ar.itc" : "=r"(x) :: "memory");
    while (__builtin_expect ((int) x == -1, 0))
        __asm__ __volatile__("mov %0=ar.itc" : "=r"(x) :: "memory");
    return x;
#else
   
    unsigned long long x;
    unsigned int low,high;
    __asm__ __volatile__("rdtsc" : "=a" (low), "=d" (high));
    x = high;
    x = (x<<32)+low;
    return x;
#endif
}*/
